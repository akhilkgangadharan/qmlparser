#include <QGuiApplication>
#include "qmlparser.h"

int main(int argc, char *argv[])
{
    QGuiApplication a(argc, argv);

    QmlParser *parser = new QmlParser;

    parser->loadFile("/path/to/qml");
    parser->writeMetaData();
    parser->readMetaData();
    return a.exec();
}
