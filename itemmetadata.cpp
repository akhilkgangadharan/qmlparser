#include "itemmetadata.h"

static QStringList visualQMLClasses = { "QQuickRectangle", "QQuickText", "Image", "QQuickSequentialAnimation", "QQuickParallelAnimation", "QQuickNumberAnimation", "QQuickRectangle_QML_0", "QQuickImage" };

QVariant getValue(QObject* object, const QString &propertyName, int propertyIndex)
{
    QMetaProperty prop = object->metaObject()->property(propertyIndex);
    if(QString::compare(prop.name(), propertyName) == 0) {
        QVariant propertyValue = prop.read(object);
        if(propertyValue.isValid()) {
            return propertyValue;
        }
    }
    return QVariant();
}

void TransformMetadata::init(QObject* transform)
{
        if(!transform)
            return;
        QString className = transform->metaObject()->className();

        foreach(QObject* child, transform->children())
            transformTraversal(child);
}

void TransformMetadata::transformTraversal(QObject* transform)
{
    if(!transform)
        return;

    QString className = transform->metaObject()->className();
    if(className.contains("QQuickRotation")) {
        QVariant axis = getValue(transform, "axis", 3);
        QVector3D axisvalues = axis.value<QVector3D>();
        rotation.axisX = axisvalues.x();
        rotation.axisY = axisvalues.y();
        rotation.axisZ = axisvalues.z();

        QVariant origin = getValue(transform, "origin", 1);
        QVector3D originvalues = origin.value<QVector3D>();
        rotation.originX = originvalues.x();
        rotation.originY = originvalues.y();

        rotation.angle = getValue(transform, "angle", 2).toFloat();
    }
    else if(className.contains("QQuickScale")) {
        QVariant origin = getValue(transform, "origin", 1);
        QVector3D originvalues = origin.value<QVector3D>();
        scale.originX = originvalues.x();
        scale.originY = originvalues.y();
        scale.xScale = getValue(transform, "xScale", 2).toFloat();
        scale.yScale = getValue(transform, "yScale", 3).toFloat();
    }
    else if(className.contains("QQuickTranslate")){
        translate.x = getValue(transform, "x", 1).toFloat();
        translate.y = getValue(transform, "y", 2).toFloat();
    }
}

QString TransformMetadata::prepareFormat()
{
    // note: type conversion here is inevitable as metaObject returns QVector3D which takes float as parameters
    QString data = "transform," + QString::number(rotation.axisX)  + "," + QString::number(rotation.axisY) + "," + QString::number(rotation.axisZ) + "," +
                    QString::number(rotation.originX)  + "," + QString::number(rotation.originY) + "," + QString::number(rotation.angle) + "," +
                    QString::number(scale.originX) + "," + QString::number(scale.originY) + "," + QString::number(scale.xScale) + "," + QString::number(scale.yScale) + "," +
                    QString::number(translate.x) + "," + QString::number(translate.y) + "," + "\n";
    return data;
}

void AnchorsGroup::init(QObject *rootObject)
{
    QObject* anchorsGroup = qvariant_cast<QObject *>(getValue(rootObject, "anchors", 18));
    margins = getValue(anchorsGroup, "margins", 8).toReal();
    topMargin = getValue(anchorsGroup, "topMargin", 12).toReal();
    bottomMargin = getValue(anchorsGroup, "bottomMargin", 13).toReal();
    leftMargin = getValue(anchorsGroup, "leftMargin", 9).toReal();
    rightMargin = getValue(anchorsGroup, "rightMargin", 10).toReal();
    baselineOffset = getValue(anchorsGroup, "baselineOffset", 15).toReal();
    verticalCenterOffset = getValue(anchorsGroup, "verticalCenterOffset", 14).toReal();
    horizontalCenterOffset = getValue(anchorsGroup, "horizontalCenterOffset", 11).toReal();
    alignWhenCentered = getValue(anchorsGroup, "alignWhenCentered", 18).toBool();
    fill = getValue(anchorsGroup, "fill", 16).toString();
    centerIn = getValue(anchorsGroup, "centerIn", 17).toString();
}

QString AnchorsGroup::prepareFormat()
{
    QString data = "anchorsGroup," + QString::number(margins) + "," + QString::number(topMargin)  + "," + QString::number(bottomMargin) + "," + QString::number(leftMargin)  + "," +
                    QString::number(rightMargin) + "," + QString::number(baselineOffset) + "," + QString::number(horizontalCenterOffset) + "," +
                    QString::number(horizontalCenterOffset) + "," + QString::number(verticalCenterOffset) + "," + fill + "," + centerIn + "," +
                    QString::number(alignWhenCentered) + "," + "\n";
    return data;
}

void ItemMetadata::init(QObject* rootObject)
{
    // The property indices are hardcoded for sake of efficiency, determined by how the metaObject orders it (which doesn't change)
    // For debugging purposes, you may output metaObject->className() within a loop from 0 till metaObject->propertyCount()
    id = getValue(rootObject, "objectName", 0).toString();
    x = getValue(rootObject, "x", 5).toReal();
    y = getValue(rootObject, "y", 6).toReal();
    z = getValue(rootObject, "z", 7).toReal();
    width = getValue(rootObject, "width", 8).toReal();
    height = getValue(rootObject, "height", 9).toReal();
    scale = getValue(rootObject, "scale", 32).toReal();
    opacity = getValue(rootObject, "opacity", 10).toReal();
    rotation = getValue(rootObject, "rotation", 31).toReal();
    transformOrigin = getValue(rootObject, "transformOrigin", 33).toString();
    antialiasing = getValue(rootObject, "antialiasing", 37).toBool();
    visible = getValue(rootObject, "visible", 12).toBool();
    smooth = getValue(rootObject, "smooth", 36).toBool();
    anchors.init(rootObject);
    transform.init(rootObject);
    int count = 0;
    QObjectList childrenList = rootObject->children();
    foreach(QObject* child, childrenList) {
        // Ensure we don't count non essential qml classes
        // TODO: check if childCount is same as calling QQuickItem::visibleChildren()
        if(visualQMLClasses.contains(child->metaObject()->className())) {
                count++;
        }
    }
    childCount = count;
}

QString ItemMetadata::prepareFormat()
{
    QString line1 = "className," + className + "\n";
    QString line2 = "childrenCount," + QString::number(childCount) + "\n";
    QString line3 = anchors.prepareFormat();
    QString line4 = transform.prepareFormat();
    QString line5 = "values_int,\n";
    QString line6 = "values_real," + QString::number(x) + "," + QString::number(y)  + ","
            + QString::number(z)  + "," + QString::number(width)  + "," + QString::number(height) + ","
            + QString::number(opacity) + "," + QString::number(scale) + "," + QString::number(rotation) + ",\n";
    QString line7 = "values_string," + transformOrigin + ",\n";
    QString line8 = "values_bool," + QString::number(visible) + "," + QString::number(antialiasing) + "," + QString::number(smooth) + ",\n";
    QString finalData;
    // avoid temporaries. It is slightly more efficient to call QString::reserve() and then append the strings to the final data
    finalData.reserve(line1.length() + line2.length() + line3.length() + line4.length() + line5.length() + line6.length()
                      + line7.length() + line8.length());
    finalData.append(line1);
    finalData.append(line2);
    finalData.append(line3);
    finalData.append(line4);
    finalData.append(line5);
    finalData.append(line6);
    finalData.append(line7);
    finalData.append(line8);
    return finalData;
}

void RectangleMetadata::init(QObject *rootObject)
{
    item.className = "QQuickRectangle";
    item.init(rootObject);
    radius = getValue(rootObject, "radius", 45).toReal();
    color = getValue(rootObject, "color", 42).toString();
    // Rectangle border is drawn by QQuickPen (an internal class)
    QObject* quickPen = qvariant_cast<QObject *>(getValue(rootObject, "border", 44));
    borderWidth = getValue(quickPen, "width", 1).toInt();
    borderColor = getValue(quickPen, "color", 2).toString();

    // gradient data is stored as a QJSValue which is then typecasted to QObject to obtain metadata from
    QJSValue quickGradient =  qvariant_cast<QJSValue>(getValue(rootObject, "gradient", 43));
    QObject *gradient = quickGradient.toQObject();
    gradientData = parseGradient(gradient);
}

QString RectangleMetadata::parseGradient(QObject *rootObject)
{
    QString orientation = getValue(rootObject, "orientation", 2).toString();
    QString final = orientation + "," +  gradientTraversal(rootObject);
    return final;
}

QString RectangleMetadata::gradientTraversal(QObject *gradient)
{
    if(!gradient)
        return QString();

    QString className = gradient->metaObject()->className();
    if(className == QString("QQuickGradient")) {
        QString final = "";
        foreach(QObject* child, gradient->children()) {
            final += gradientTraversal(child);
        }
        return final;
    }
    else if(className.contains("Stop")) {
        QString position = getValue(gradient, "position", 1).toString();
        QString color = getValue(gradient, "color", 2).toString();
        return QString(position + "," + color + ",");
    }
    else {
        qDebug() << "WARNING: Unexpected class in parsing gradient - " << className;
        return QString();
    }
}

QString RectangleMetadata::prepareFormat()
{
    QString itemFormat = item.prepareFormat();
    QString line1 = "gradient," + gradientData + "\n";
    QString line2 = "values_int,\n";
    QString line3 = "values_real," + QString::number(radius) + "," + QString::number(borderWidth) + "," + "\n";
    QString line4 = "values_string," + color + "," + borderColor + "," + "\n";
    QString line5 = "values_bool,\n";

    QString finalData;
    finalData.reserve(itemFormat.length() + line1.length() + line2.length() + line3.length() + line4.length() + line5.length());
    finalData.append(itemFormat);
    finalData.append(line1);
    finalData.append(line2);
    finalData.append(line3);
    finalData.append(line4);
    finalData.append(line5);
    return finalData;
}

void TextMetadata::init(QObject *rootObject)
{
    item.className = "QQuickText";
    QFont font = qvariant_cast<QFont>(getValue(rootObject, "font", 45));
    fontFamily = font.family();
    fontPointSize = font.pointSize();
    fontPixelSize = font.pixelSize();
    fontBold = font.bold();
    fontItalic = font.italic();
    fontUnderline = font.underline();
    fontStrikeout = font.strikeOut();
    fontKerning = font.kerning();
    fontLetterSpacing = font.letterSpacing();
    fontWordSpacing = font.wordSpacing();
    QMetaEnum styleStrategyMeta = QMetaEnum::fromType<QFont::StyleStrategy>();
    QString styleStrategyString = styleStrategyMeta.valueToKey(font.styleStrategy());

    /*
    int fontStyleName_value = font.styleName().toInt();
    if(fontStyleName_value == 0) {
        fontStyleName = "Font.Thin";
    }
    else if(fontStyleName_value == 25) {
        fontStyleName = "Font.Light";
    }
    else if(fontStyleName_value == 12) {
        fontStyleName = "Font.ExtraLight";
    }
    else if(fontStyleName_value == 50) {
        fontStyleName = "Font.Normal";
    }
    else if(fontStyleName_value == 57) {
        fontStyleName = "Font.Medium";
    }
    else if(fontStyleName_value == 63) {
        fontStyleName = "Font.DemiBold";
    }
    else if(fontStyleName_value == 75) {
        fontStyleName = "Font.Bold";
    }
    else if(fontStyleName_value == 81) {
        fontStyleName = "Font.ExtraBold";
    }
    else if(fontStyleName_value == 87) {
        fontStyleName = "Font.Black";
    }
    */

    int fontWeight_value = font.weight();
    if(fontWeight_value == 0) {
        fontWeight = "Font.Thin";
    }
    else if(fontWeight_value == 25) {
        fontWeight = "Font.Light";
    }
    else if(fontWeight_value == 12) {
        fontWeight = "Font.ExtraLight";
    }
    else if(fontWeight_value == 50) {
        fontWeight = "Font.Normal";
    }
    else if(fontWeight_value == 57) {
        fontWeight = "Font.Medium";
    }
    else if(fontWeight_value == 63) {
        fontWeight = "Font.DemiBold";
    }
    else if(fontWeight_value == 75) {
        fontWeight = "Font.Bold";
    }
    else if(fontWeight_value == 81) {
        fontWeight = "Font.ExtraBold";
    }
    else if(fontWeight_value == 87) {
        fontWeight = "Font.Black";
    }

    if(styleStrategyString.contains("Default")) {
        fontPreferShaping = true;
    }
    else {
        fontPreferShaping = false;
    }

    int fontCap_val= font.capitalization();
    if(fontCap_val == 0) {
        fontCapitalization = "Font.MixedCase";
    }
    else if(fontCap_val == 1) {
        fontCapitalization = "Font.AllUppercase";
    }
    else if(fontCap_val == 2) {
        fontCapitalization = "Font.AllLowercase";
    }
    else if(fontCap_val == 3) {
        fontCapitalization = "Font.SmallCaps";
    }
    else if(fontCap_val == 4) {
        fontCapitalization = "Font.Capitalize";
    }

    // other font properties
    int style_value = getValue(rootObject, "style", 48).toInt();
    if(style_value == 0) {
        style = "Text.Normal";
    }
    else if(style_value == 1) {
        style = "Text.Outline";
    }
    else if(style_value == 2) {
        style = "Text.Raised";
    }
    else if(style_value == 3) {
        style = "Text.Sunken";
    }

    styleColor = getValue(rootObject, "styleColor", 49).toString();
    int hor_value = getValue(rootObject, "horizontalAlignment", 50).toInt();
    if(hor_value == 1) {
        horizontalAlignment = "Text.AlignLeft";
    }
    else if(hor_value == 2) {
        horizontalAlignment = "Text.AlignRight";
    }
    else if(hor_value == 4) {
        horizontalAlignment = "Text.AlignHCenter";
    }
    else if(hor_value == 8) {
        horizontalAlignment = "Text.AlignJustify";
    }

    int vert_value = getValue(rootObject, "verticalAlignment", 52).toInt();
    if(vert_value == 32) {
        verticalAlignment = "Text.AlignTop";
    }
    else if(vert_value == 64) {
        verticalAlignment = "Text.AlignBottom";
    }
    else if(vert_value == 128) {
        verticalAlignment = "Text.AlignVCenter";
    }

    int elide_value = getValue(rootObject, "elide", 58).toInt();
    if(elide_value == 3) {
        elide = "Text.ElideNone";
    }
    else if(elide_value == 0) {
        elide = "Text.ElideLeft";
    }
    else if(elide_value == 2) {
        elide = "Text.ElideMiddle";
    }
    else if(elide_value == 1) {
        elide = "Text.ElideRight";
    }

    int fSizeMode_value = getValue(rootObject, "fontSizeMode", 68).toInt();
    if(fSizeMode_value == 0) {
        fontSizeMode = "Text.FixedSize";
    }
    else if(fSizeMode_value == 1) {
        fontSizeMode = "Text.HorizontalFit";
    }
    else if(fSizeMode_value == 2) {
        fontSizeMode = "Text.VerticalFit";
    }
    else if(fSizeMode_value == 3) {
        fontSizeMode = "Text.Fit";
    }

    int textFormat_value = getValue(rootObject, "textFormat", 57).toInt();
    if(textFormat_value == 2) {
        textFormat = "Text.AutoText";
    }
    else if(textFormat_value == 0) {
        textFormat = "Text.PlainText";
    }
    else if(textFormat_value == 4) {
        textFormat = "Text.StyledText";
    }
    else if(textFormat_value == 1) {
        textFormat = "Text.RichText";
    }
    else if(textFormat_value == 3) {
        textFormat = "Text.MarkdownText";
    }

    int wrapMode_value = getValue(rootObject, "wrapMode", 53).toInt();
    if(wrapMode_value == 0) {
        wrapMode = "Text.NoWrap";
    }
    else if(wrapMode_value == 1) {
        wrapMode = "Text.WordWrap";
    }
    else if(wrapMode_value == 3) {
        wrapMode = "Text.WrapAnywhere";
    }
    else if(wrapMode_value == 4) {
        wrapMode = "Text.Wrap";
    }

    int renderType_value = getValue(rootObject, "renderType", 69).toInt();
    if(renderType_value == 0) {
        renderType = "Text.QtRendering";
    }
    else if(renderType_value == 1) {
        renderType = "Text.NativeRendering";
    }

    color = getValue(rootObject, "color", 46).toString();
    text = getValue(rootObject, "text", 44).toString();
    // We need to preserve "\n" in text so replace them with "\\n" and when creating text item, replace "\\n" with "\n"
    QString replacenewline = "\\n";
    text.replace(QString("\n"), replacenewline);

    lineHeight = getValue(rootObject, "lineHeight", 63).toReal();
    padding = getValue(rootObject, "padding", 71).toReal();
    topPadding = getValue(rootObject, "topPadding", 72).toReal();
    leftPadding = getValue(rootObject, "leftPadding", 73).toReal();
    rightPadding = getValue(rootObject, "rightPadding", 74).toReal();
    bottomPadding = getValue(rootObject, "bottomPadding", 75).toReal();

    foreach(QObject* child, rootObject->children()) {
        QString className = child->metaObject()->className();
        if(className.contains("FontLoader")) {
            fontLoaderSource = getValue(rootObject, "source", 1).toString();
            QVariant i = getValue(child, "objectName", 0);
            QString objectName = i.value<QString>();
            fontLoaderName = objectName + ".name";
            break;
        }
    }
}

QString TextMetadata::prepareFormat()
{
    QString itemData = item.prepareFormat();
    QString line0 = text + "\n";
    QString line1 = "values_int," + QString::number(fontPixelSize) + "," + "\n";
    QString line2 = "values_real," + QString::number(fontPointSize) + "," + QString::number(lineHeight) + ","
                    + QString::number(fontLetterSpacing) + "," + QString::number(fontWordSpacing) + "," + QString::number(padding) + "," + QString::number(bottomPadding) + ","
                    + QString::number(topPadding) + "," + QString::number(leftPadding) + "," + QString::number(rightPadding) + "," + "\n";
    QString line3 = "values_string," + color + "," + elide + "," + fontFamily + "," + fontSizeMode + ","
                                     + fontCapitalization + "," + style + "," + styleColor
                                     + "," + textFormat + "," + wrapMode + "," + horizontalAlignment
                                     + "," + verticalAlignment + "," + renderType + "," + fontWeight
                                     + "," + fontLoaderName + "," + fontLoaderSource + ",\n";
    QString line4 = "values_bool," + QString::number(fontBold) + "," + QString::number(fontItalic) + "," + QString::number(fontStrikeout)
                                    + "," + QString::number(fontUnderline) + "," + QString::number(fontKerning) + "," + QString::number(fontPreferShaping) + "," + QString::number(ifFontLoader) + ",\n";
    QString final;
    final.reserve(itemData.length() + line0.length() + line1.length() + line2.length() + line3.length() + line4.length());
    final.append(itemData);
    final.append(line0);
    final.append(line1);
    final.append(line2);
    final.append(line3);
    final.append(line4);
    return final;
}

void ImageMetadata::init(QObject *rootObject)
{
    item.className = "QQuickImage";
    item.init(rootObject);
    source = getValue(rootObject, "source", 45).toString();

    int hor_value = getValue(rootObject, "horizontalAlignment", 56).toInt();
    if(hor_value == 1) {
        horizontalAlignment = "Image.AlignLeft";
    }
    else if(hor_value == 2) {
        horizontalAlignment = "Image.AlignRight";
    }
    else if(hor_value == 4) {
        horizontalAlignment = "Image.AlignHCenter";
    }
    else if(hor_value == 8) {
        horizontalAlignment = "Image.AlignJustify";
    }

    int vert_value = getValue(rootObject, "verticalAlignment", 57).toInt();
    if(vert_value == 32) {
        verticalAlignment = "Image.AlignTop";
    }
    else if(vert_value == 64) {
        verticalAlignment = "Image.AlignBottom";
    }
    else if(vert_value == 128) {
        verticalAlignment = "Image.AlignVCenter";
    }

    int fillMode_value = getValue(rootObject, "fillMode", 53).toInt();
    if(fillMode_value == 0) {
        fillMode = "Image.Stretch";
    }
    else if(fillMode_value == 1) {
        fillMode = "Image.PreserveAspectFit";
    }
    else if(fillMode_value == 2) {
        fillMode = "Image.PreserveAspectCrop";
    }
    else if(fillMode_value == 3) {
        fillMode = "Image.Tile";
    }
    else if(fillMode_value == 4) {
        fillMode = "Image.TileVertically";
    }
    else if(fillMode_value == 5) {
        fillMode = "Image.TileHorizontally";
    }
    else if(fillMode_value == 6) {
        fillMode = "Image.Pad";
    }
    asynchronous = getValue(rootObject, "asynchronous", 47).toBool();
    autoTransform = getValue(rootObject, "autoTransform", 59).toBool();
    cache = getValue(rootObject, "cache", 48).toBool();
    mipmap = getValue(rootObject, "mipmap", 58).toBool();
    mirror = getValue(rootObject, "mirror", 50).toBool();
}

QString ImageMetadata::prepareFormat()
{
    QString itemData = item.prepareFormat();
    QString line1 = "values_int," + QString::number(sourceSizeWidth) + "," + QString::number(sourceSizeHeight) + ",\n";
    QString line2 = "values_real,\n";
    QString line3 = "values_string," + source + "," + horizontalAlignment + "," + verticalAlignment + "," + fillMode + "," + "\n";
    QString line4 = "values_bool," + QString::number(asynchronous) + "," + QString::number(autoTransform) + "," + QString::number(cache) + "," + QString::number(mipmap) + "," + QString::number(mirror) + "," + "\n";
    QString final;
    final.reserve(itemData.length() + line1.length() + line2.length() + line3.length() + line4.length());
    final.append(itemData);
    final.append(line1);
    final.append(line2);
    final.append(line3);
    final.append(line4);
    return final;
}
