#ifndef QMLPARSER_H
#define QMLPARSER_H

#include <QObject>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QFile>
#include <QQuickItem>
#include <QTemporaryFile>
#include <QQuickView>
#include <QQmlContext>
#include <QQuickRenderControl>

#include "itemmetadata.h"


class QmlParser : public QObject
{
    Q_OBJECT
public:
    explicit QmlParser(QObject *parent = nullptr);
    ~QmlParser() = default;

    void loadFile(const QString path);
    void setQmlEngine(QQmlEngine* engine);
    QObject* getRootItem() { return m_qmlRootItem; }
    void writeMetaData();
    void readMetaData();
    void deleteMetaData();
    void printMetaData();
    void printTraversal(QObject*);


private:
    void writeTraversal(QObject*);
    void readTraversal(QQuickItem*);

    int m_objectCounter;
    QQmlEngine *m_qmlEngine;
    QQmlComponent *m_qmlComponent;
    QQuickItem *m_qmlRootItem;
    QTemporaryFile m_quickFile;
    QFile *m_qmlFile;
    QTextStream m_textStream;
    QString m_qmlFilePath;

    QQuickItem* createEmptyItem();
    QQuickItem* createRectangle(RectangleMetadata);
    QQuickItem* createTextItem(TextMetadata);
    QQuickItem* createImageItem(ImageMetadata);
    void viewItem(QQuickItem*);
    void addItem(QQuickItem*);
};

QVariant getObjectPropertyValue(QObject* object, const QString &propertyName);


#endif // QMLPARSER_H
