#include "qmlparser.h"

QmlParser::QmlParser(QObject *parent)
    : QObject(parent)
    , m_qmlEngine(nullptr)
    , m_qmlComponent(nullptr)
    , m_objectCounter(0)
{
    m_qmlEngine = new QQmlEngine();
}

static bool openAsQFile(QFile &file, QIODevice::OpenMode mode)
{
    return file.open(mode);
}

void QmlParser::loadFile(const QString path)
{
    // Load the qml
    if(!path.contains("qml") && !QFile(path).exists()) {
        qDebug() << " Incorrect QML Path\n";
        return;
    }

    m_qmlFilePath = path;
    m_qmlFile = new QFile(m_qmlFilePath);

    m_qmlComponent = new QQmlComponent(m_qmlEngine, path, QQmlComponent::PreferSynchronous);

    if (m_qmlComponent->isError()) {
        const QList<QQmlError> errorList = m_qmlComponent->errors();
        for (const QQmlError &error : errorList)
            qDebug() << "QML Component Error: " << error.url() << error.line() << error;
        return;
    }

    // Get the root object
    QObject *rootObject = m_qmlComponent->create();
    m_qmlRootItem = qobject_cast<QQuickItem*>(rootObject);

}

void QmlParser::writeMetaData()
{
    m_objectCounter = 0;
    // delete previous metadata, if any
    deleteMetaData();
    if(!openAsQFile(m_quickFile, QIODevice::WriteOnly | QIODevice::Append)) {
        qDebug() << " Temporary File could not be created \n";
        return;
    }

    m_quickFile.setTextModeEnabled(true);

    writeTraversal(m_qmlRootItem);
    m_quickFile.close();

    if(!m_quickFile.open()) {
        qDebug() << " Temporary File could not be created \n";
        return;
    }
    QFile qmlFile(m_qmlFilePath);
    QTextStream stream(&qmlFile);

    if(!qmlFile.open(QIODevice::WriteOnly | QIODevice::Append)) {
        qDebug() << " Failed attempt on opening " << m_qmlFilePath;
        return;
    }
    qmlFile.write("/*\n");
    while(!m_quickFile.atEnd()) {
        QByteArray line = m_quickFile.readLine();
        qmlFile.write(line);
    }
    qmlFile.write("*/");
    qmlFile.close();
    m_quickFile.close();
}

void QmlParser::writeTraversal(QObject *rootObject)
{
    if(!rootObject) {
        return;
    }
    QTextStream stream(&m_quickFile);
    QString objectName = rootObject->metaObject()->className();

    if(objectName.contains("QQuickItem")) {
        m_objectCounter++;
        ItemMetadata item;
        item.className = "QQuickItem";
        item.init(rootObject);
        QString final = item.prepareFormat();
        stream << final;
        stream.flush();

        foreach(QObject* child, rootObject->children()) {
            writeTraversal(child);
        }
    }
    else if(objectName.contains("Rectangle")) {
        m_objectCounter++;
        RectangleMetadata rect;
        rect.init(rootObject);
        stream << rect.prepareFormat();
        stream.flush();

        foreach(QObject* child, rootObject->children()) {
            writeTraversal(child);
        }
    }
    else if(objectName.contains("QQuickShape")) {
        m_objectCounter++;
    }
    else if(objectName.contains("Text")) {
        m_objectCounter++;

        TextMetadata text;
        text.init(rootObject);
        stream << text.prepareFormat();

        foreach(QObject* child, rootObject->children()) {
            writeTraversal(child);
        }
    }
    else if(objectName.contains("Image")) {
        m_objectCounter++;
        ImageMetadata img;

        img.init(rootObject);
        stream << img.prepareFormat();
        stream.flush();

        foreach(QObject* child, rootObject->children()) {
          writeTraversal(child);
        }
    }
    else if(objectName.contains("Animation")) {
        // TODO
        m_objectCounter++;
        if(objectName.contains("Sequential") || objectName.contains("Paralell")) {

        }
        if(objectName.contains("Number")) {

        }
    }
    else {
        foreach(QObject* child, rootObject->children()) {
            writeTraversal(child);
        }
    }
}

void QmlParser::readMetaData()
{
    if(!m_qmlFile->open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << " Failed attempt on opening " << m_qmlFilePath;
        return;
    }
    // Traverse till the point where we need to start reading metadata
    while(!m_qmlFile->atEnd()) {
        QString line = m_qmlFile->readLine();
        if(line.startsWith("/*")) {
            break;
        }
    }

    QQuickItem *parentItem = createEmptyItem();

    readTraversal(parentItem);
    m_qmlFile->close();
    viewItem(parentItem);
}

void QmlParser::deleteMetaData()
{
    if(!m_qmlFile->open(QIODevice::ReadWrite | QIODevice::Text)) {
        qDebug() << " Failed attempt on opening " << m_qmlFilePath;
        return;
    }

    QString modifiedContent;
    QTextStream stream(m_qmlFile);
    while(!stream.atEnd())
    {
        QString line = stream.readLine();
        if(line.contains("/*"))
            break;
        else
            modifiedContent.append(line + "\n");
    }
    m_qmlFile->resize(0);
    stream << modifiedContent;
    m_qmlFile->close();

}

void QmlParser::readTraversal(QQuickItem *parentItem)
{
    if(m_qmlFile->atEnd()) {
        qDebug() << " PARSING ERROR: FILE POINTER AT END\n";
        return  ;
    }

    ItemMetadata item;
    // Parse metadata from qml file
    QString line =  m_qmlFile->readLine();
    // Get the className
    if(line.contains("className")) {
        QStringList data = line.split(",");
        item.className = data[1];

        // Get the child count
        line =  m_qmlFile->readLine();
        if(!line.contains("childrenCount")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN childrenCount\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        item.childCount = data[1].toInt();

        // Get the anchors
        line =  m_qmlFile->readLine();
        if(!line.contains("anchorsGroup")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN anchorsGroup\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        data.removeFirst();

        item.anchors.margins = data[0].toDouble();
        item.anchors.topMargin = data[1].toDouble();
        item.anchors.bottomMargin = data[2].toDouble();
        item.anchors.leftMargin = data[3].toDouble();
        item.anchors.rightMargin = data[4].toDouble();
        item.anchors.baselineOffset = data[5].toDouble();
        item.anchors.horizontalCenterOffset = data[6].toDouble();
        item.anchors.verticalCenterOffset = data[7].toDouble();
        item.anchors.fill = data[8];
        item.anchors.centerIn = data[9];
        item.anchors.alignWhenCentered = (data[10].toInt() == 1? true: false);

        // Get transform
        line =  m_qmlFile->readLine();
        if(!line.contains("transform")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN transform\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        data.removeFirst();

        item.transform.rotation.axisX = data[0].toFloat();
        item.transform.rotation.axisY = data[1].toFloat();
        item.transform.rotation.axisZ = data[2].toFloat();
        item.transform.rotation.originX = data[3].toFloat();
        item.transform.rotation.originY = data[4].toFloat();
        item.transform.rotation.angle = data[5].toFloat();

        item.transform.scale.originX = data[6].toFloat();
        item.transform.scale.originY = data[7].toFloat();
        item.transform.scale.xScale = data[8].toFloat();
        item.transform.scale.yScale = data[9].toFloat();

        item.transform.translate.x = data[10].toFloat();
        item.transform.translate.y = data[11].toFloat();

        // Get the list of int values
        // currently empty
        line =  m_qmlFile->readLine();
        if(!line.contains("values_int")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN value_int\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        data.removeFirst();

        // Get the list of real values
        line =  m_qmlFile->readLine();
        if(!line.contains("values_real")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_real\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        data.removeFirst();

        item.x = data[0].toDouble();
        item.y = data[1].toDouble();
        item.z = data[2].toDouble();
        item.width = data[3].toDouble();
        item.height = data[4].toDouble();
        item.opacity = data[5].toDouble();
        item.scale = data[6].toDouble();
        item.rotation = data[7].toDouble();

        // Get the list of string values
        line =  m_qmlFile->readLine();
        if(!line.contains("values_string")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_string\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        data.removeFirst();

        item.transformOrigin = data[0];

        // Get the list of bool values
        line =  m_qmlFile->readLine();
        if(!line.contains("values_bool")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_bool\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        data.removeFirst();

        item.visible = (data[0].toInt() == 1? true: false);
        item.antialiasing = (data[1].toInt() == 1? true: false);
        item.smooth = (data[2].toInt() == 1? true: false);
    }
    else {
        qDebug() << " PARSING ERROR: UNEXPECTED LINE IN className";
        qDebug() << " >> PARSING ERROR in line: " << line;
        return;
    }

    if(item.className.contains("Item")) {
            // assign values to root quickitem
            parentItem->setX(item.x);
            parentItem->setY(item.y);
            parentItem->setZ(item.z);
            parentItem->setWidth(item.width);
            parentItem->setHeight(item.height);
            parentItem->setOpacity(item.opacity);
            parentItem->setScale(item.scale);
            parentItem->setRotation(item.rotation);
            // TODO
            /*
            QMetaEnum transform = QMetaEnum::fromType<QQuickItem::TransformOrigin>();
            const QString val = item.transformOrigin;
            bool ok = false;
            transform.keyToValue(item.transformOrigin.toStdString().c_str(), &ok);
            parentItem->setTransformOrigin(transform.value());
            */
            parentItem->setVisible(item.visible);
            parentItem->setAntialiasing(item.antialiasing);
            parentItem->setSmooth(item.smooth);

            for(int i = 0; i < item.childCount; i++) {
                readTraversal(parentItem);
            }
    }
    else if(item.className.contains("Rectangle")) {
        RectangleMetadata rect;
        rect.item = item;

        QString line;
        QStringList data;

        line =  m_qmlFile->readLine();
        if(!line.contains("gradient")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_real\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        rect.gradientData = line;

        // values_int is empty
        line =  m_qmlFile->readLine();
        if(!line.contains("values_int")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_int\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }

        line =  m_qmlFile->readLine();
        if(!line.contains("values_real")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_real\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        data.removeFirst();
        rect.radius = data[0].toDouble();
        rect.borderWidth = data[1].toInt();

        line =  m_qmlFile->readLine();
        if(!line.contains("values_string")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_string\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        data.removeFirst();
        rect.color = data[0];
        rect.borderColor = data[1];

        // values_bool is empty
        line =  m_qmlFile->readLine();
        if(!line.contains("values_bool")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_bool\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }

        QQuickItem* rectangleItem = createRectangle(rect);
        rectangleItem->setParent(parentItem);
        rectangleItem->setParentItem(parentItem);
        if(item.childCount > 0) {
            for(int i = 0; i < item.childCount; i++) {
                readTraversal(rectangleItem);
            }
        }
        else {
            return;
        }
    }
    else if(item.className.contains("Text")) {
        TextMetadata text;
        text.item = item;
        QString line;
        QStringList data;

        line =  m_qmlFile->readLine();
        text.text = line;

        line =  m_qmlFile->readLine();
        if(!line.contains("values_int")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_real\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        data.removeFirst();
        text.fontPixelSize = data[0].toInt();

        line =  m_qmlFile->readLine();
        if(!line.contains("values_real")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_real\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        data.removeFirst();
        text.fontPointSize = data[0].toDouble();
        text.lineHeight = data[1].toDouble();
        text.fontLetterSpacing = data[2].toDouble();
        text.fontWordSpacing = data[3].toDouble();
        text.padding = data[4].toDouble();
        text.bottomPadding = data[5].toDouble();
        text.topPadding = data[6].toDouble();
        text.leftPadding = data[7].toDouble();
        text.rightPadding = data[8].toDouble();

        line =  m_qmlFile->readLine();
        if(!line.contains("values_string")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_real\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        data.removeFirst();
        text.color = data[0];
        text.elide = data[1];
        text.fontFamily = data[2];
        text.fontSizeMode = data[3];
        text.fontCapitalization = data[4];
        text.style = data[5];
        text.styleColor = data[6];
        text.textFormat = data[8];
        text.wrapMode = data[9];
        text.horizontalAlignment = data[10];
        text.verticalAlignment = data[11];
        text.renderType = data[12];
        text.fontWeight = data[13];
        text.fontLoaderName = data[14];
        text.fontLoaderSource = data[15];

        line =  m_qmlFile->readLine();
        if(!line.contains("values_bool")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_bool\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        data.removeFirst();

        text.fontBold = (data[0].toInt() == 1? true:false);
        text.fontItalic = (data[1].toInt() == 1? true:false);
        text.fontStrikeout = (data[2].toInt() == 1? true:false);
        text.fontUnderline = (data[3].toInt() == 1? true:false);
        text.fontKerning = (data[4].toInt() == 1? true:false);
        text.fontPreferShaping = (data[5].toInt() == 1? true:false);
        text.ifFontLoader = (data[6].toInt() == 1? true:false);

        QQuickItem* textItem = createTextItem(text);
        textItem->setParent(parentItem);
        textItem->setParentItem(parentItem);
        if(item.childCount > 0) {
            for(int i = 0; i < item.childCount; i++) {
                readTraversal(textItem);
            }
        }
        else {
            return;
        }
    }
    else if(item.className.contains("Image")) {
        ImageMetadata img;
        img.item = item;
        QString line;
        QStringList data;
        line =  m_qmlFile->readLine();
        if(!line.contains("values_int")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_real\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        data.removeFirst();
        img.sourceSizeWidth = data[0].toInt();
        img.sourceSizeHeight = data[1].toInt();

        line = m_qmlFile->readLine();
        if(!line.contains("values_real")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_real\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }

        line =  m_qmlFile->readLine();
        if(!line.contains("values_string")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_real\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        data.removeFirst();

        img.source= data[0];
        img.horizontalAlignment = data[1];
        img.verticalAlignment = data[2];
        img.fillMode = data[3];

        line =  m_qmlFile->readLine();
        if(!line.contains("values_bool")) {
            qDebug() << " PARSING ERROR: UNEXPECTED LINE IN values_bool\n";
            qDebug() << " >> PARSING ERROR in line: " << line;
            return;
        }
        data = line.split(",");
        data.removeFirst();

        img.asynchronous = (data[0].toInt() == 1? true:false);
        img.autoTransform = (data[1].toInt() == 1? true:false);
        img.cache = (data[2].toInt() == 1? true:false);
        img.mipmap = (data[3].toInt() == 1? true:false);
        img.mirror = (data[4].toInt() == 1? true:false);

        QQuickItem* imageItem = createImageItem(img);
        imageItem->setParentItem(parentItem);
        imageItem->setParent(parentItem);
        if(item.childCount > 0) {
            for(int i = 0; i < item.childCount; i++) {
                readTraversal(imageItem);
            }
        }
        else {
            return;
        }
    }
    else {
        for(int i = 0; i < item.childCount; i++) {
            readTraversal(parentItem);
        }
    }
}

void QmlParser::viewItem(QQuickItem* item)
{
    QQuickWindow *quickWindow = new QQuickWindow();
    item->setParentItem(quickWindow->contentItem());
    quickWindow->show();
}


QQuickItem* QmlParser::createEmptyItem()
{
    QQmlEngine* engine = new QQmlEngine();
    QQmlComponent component(engine);


    component.setData("import QtQuick 2.14; Item {}", QUrl());
    QQuickItem* childItem = qobject_cast<QQuickItem*>(component.create());

    if (!childItem) {
        qDebug() << component.errorString();
        return nullptr;
    }
    return childItem;
}

QQuickItem* QmlParser::createRectangle(RectangleMetadata rect)

{
    QQmlEngine* engine = new QQmlEngine();
    QQmlComponent component(engine);

    QString rotation = "Rotation { axis.x: " + QString::number(rect.item.transform.rotation.axisX)
            + ";axis.y:" + QString::number(rect.item.transform.rotation.axisY)
            + ";axis.z:" + QString::number(rect.item.transform.rotation.axisZ)
            + ";origin.x:" + QString::number(rect.item.transform.rotation.originX)
            + ";origin.y:" + QString::number(rect.item.transform.rotation.originY) + "},";

    QString scale = "Scale { origin.x:" + QString::number(rect.item.transform.scale.originX)
            + ";origin.y:" + QString::number(rect.item.transform.scale.originY)
            + ";xScale:" + QString::number(rect.item.transform.scale.xScale)
            + ";yScale:" + QString::number(rect.item.transform.scale.yScale) + "},";

    QString translate = "Translate { x:" + QString::number(rect.item.transform.translate.x)
            + ";y: " + QString::number(rect.item.transform.translate.y) + "}";

    QString transformList = "transform: [" + rotation + scale + translate + "]";
    QStringList gradientStopValues = rect.gradientData.split(",");
    if(gradientStopValues.last().isEmpty() || gradientStopValues.last() == QString("\n"))
        gradientStopValues.removeLast();
    if(gradientStopValues.first() == QString("gradient"))
        gradientStopValues.removeFirst();

    QString orientation;
    int orientationValue = gradientStopValues.takeFirst().toInt();
    if(orientationValue == 1) {
        orientation = "Gradient.Horizontal";
    }
    else {
        orientation = "Gradient.Vertical";
    }

    QString gradientbaseString = "gradient: Gradient { orientation: " + orientation + ";";
    QString gradientstopString = "";
    QString singlequotes = "\'";
    for(int i=0; i<gradientStopValues.length(); i+=2) {
        gradientstopString  += "GradientStop { position: " + gradientStopValues[i] + "; color: "
                        + singlequotes + gradientStopValues[i+1] + singlequotes +  "}";
    }
    gradientbaseString += gradientstopString;
    gradientbaseString += "}";

    QString border = "border.width: " + QString::number(rect.borderWidth) + ";" + "border.color: \'" + rect.borderColor + "\' ;";
    QString data = "import QtQuick 2.12; Rectangle {" + border + gradientbaseString + transformList  + "}";
    component.setData(data.toUtf8(), QUrl());
    QQuickItem* rectItem = qobject_cast<QQuickItem*>(component.create());

    if (!rectItem) {
        qDebug() << component.errorString();
        return nullptr;
    }
    rectItem->setWidth(rect.item.width);
    rectItem->setHeight(rect.item.height);
    rectItem->setX(rect.item.x);
    rectItem->setY(rect.item.y);
    rectItem->setZ(rect.item.z);
    rectItem->setOpacity(rect.item.opacity);
    rectItem->setScale(rect.item.scale);
    rectItem->setRotation(rect.item.rotation);
    rectItem->setVisible(rect.item.visible);
    rectItem->setAntialiasing(rect.item.antialiasing);
    rectItem->setSmooth(rect.item.smooth);

    rectItem->setProperty("radius", rect.radius);
    rectItem->setProperty("color", rect.color);
    return rectItem;
}

QQuickItem* QmlParser::createTextItem(TextMetadata text)
{
    QQmlEngine* engine = new QQmlEngine();
    QQmlComponent component(engine);

    QString rotation = "Rotation { axis.x: " + QString::number(text.item.transform.rotation.axisX)
            + ";axis.y:" + QString::number(text.item.transform.rotation.axisY)
            + ";axis.z:" + QString::number(text.item.transform.rotation.axisZ)
            + ";origin.x:" + QString::number(text.item.transform.rotation.originX)
            + ";origin.y:" + QString::number(text.item.transform.rotation.originY) + "},";

    QString scale = "Scale { origin.x:" + QString::number(text.item.transform.scale.originX)
            + ";origin.y:" + QString::number(text.item.transform.scale.originY)
            + ";xScale:" + QString::number(text.item.transform.scale.xScale)
            + ";yScale:" + QString::number(text.item.transform.scale.yScale) + "},";

    QString translate = "Translate { x:" + QString::number(text.item.transform.translate.x)
            + ";y: " + QString::number(text.item.transform.translate.y) + "}";

    QString transformList = "transform: [" + rotation + scale + translate + "] ";

    QString fontFam, fontLoader;
    if(text.ifFontLoader) {
        fontLoader = "FontLoader { id: " + text.fontLoaderName + " ; source: \'" + text.fontLoaderSource + "\' }";
        fontFam = "font.family: " + QString("myfont") + ".name" + " ; ";
    }
    else {
        fontFam = "font.family: \'" + text.fontFamily + "\' ; ";
    }
    QString fontBold = "font.bold: " + QString(text.fontBold? "true":"false") + " ; ";
    QString fontItalic = "font.italic: " + QString(text.fontItalic? "true":"false") + " ; ";
    QString fontStrikeout = "font.strikeout: " + QString(text.fontStrikeout? "true":"false") + " ; ";
    QString fontUnderline = "font.underline: " + QString(text.fontUnderline? "true":"false") + " ; ";
    QString fontKerning = "font.kerning: " + QString(text.fontKerning? "true":"false") + " ;";
    QString fontPreferShaping = "font.preferShaping: " + QString(text.fontPreferShaping? "true":"false") + " ; ";
    QString fontPointSize = "font.pointSize: " + QString::number(text.fontPointSize) + "; ";
    QString fontPixelSize = "font.pixelSize: " + QString::number(text.fontPixelSize) + "; ";
    QString fontWrapMode = "wrapMode: " + text.wrapMode + " ; ";
    QString textFormat = "textFormat: " + text.textFormat + " ; ";
    QString horizontalAlignment = "horizontalAlignment: " + text.horizontalAlignment + " ; ";
    QString verticalAlignment = "verticalAlignment: " + text.verticalAlignment + " ; ";
    QString style = "style: " + text.style + " ;";
    QString renderType = "renderType: " + text.renderType + " ; ";
    QString elide = "elide: " + text.elide + " ; ";
    QString fontSizeMode = "fontSizeMode: " + text.fontSizeMode + " ; ";
    QString fontWeight = "font.weight: " + text.fontWeight + " ;";

    QString finalText = text.text.replace(QString("\\n"), "\n");
    if(finalText.back() == QString("\n")) {
        finalText.remove(finalText.length() - 1, 1);
    }
    QString textContent = "text: \'" + finalText + "\'; ";

    QString final = "import QtQuick 2.12; Text { ";
    if(text.ifFontLoader) {
        final.append(fontLoader);
    }
    final = final + textContent + transformList + fontFam + fontBold + fontItalic + fontStrikeout + fontUnderline +
            fontKerning + fontPreferShaping + fontPointSize + fontPixelSize + fontWeight + fontWrapMode + textFormat +
            horizontalAlignment + verticalAlignment + style + elide + fontSizeMode + renderType + "}";
    component.setData(final.toStdString().c_str(), QUrl());
    QQuickItem* textItem = qobject_cast<QQuickItem*>(component.create());

    if (!textItem) {
        qDebug() << component.errorString();
        return nullptr;
    }

    textItem->setWidth(text.item.width);
    textItem->setHeight(text.item.height);
    textItem->setX(text.item.x);
    textItem->setY(text.item.y);
    textItem->setZ(text.item.z);
    textItem->setOpacity(text.item.opacity);
    textItem->setScale(text.item.scale);
    textItem->setRotation(text.item.rotation);
    textItem->setVisible(text.item.visible);
    textItem->setAntialiasing(text.item.antialiasing);
    textItem->setSmooth(text.item.smooth);

    textItem->setProperty("color", text.color);
    textItem->setProperty("lineHeight", text.lineHeight);
    textItem->setProperty("padding", text.padding);
    textItem->setProperty("bottomPadding", text.bottomPadding);
    textItem->setProperty("topPadding", text.topPadding);
    textItem->setProperty("leftPadding", text.leftPadding);
    textItem->setProperty("rightPadding", text.rightPadding);

    textItem->setProperty("styleColor", text.styleColor);


    return textItem;
}

QQuickItem* QmlParser::createImageItem(ImageMetadata img)
{
    QQmlEngine* engine = new QQmlEngine();
    QQmlComponent component(engine);

    QString fillMode = "fillMode: " + img.fillMode + " ; ";
    QString horizontalAlignment = "horizontalAlignment: " + img.horizontalAlignment + " ; ";
    QString verticalAlignment = "verticalAlignment: " + img.verticalAlignment + " ; ";
    QString data = "import QtQuick 2.12; Image { " + fillMode + horizontalAlignment + verticalAlignment + "}";
    component.setData(data.toStdString().c_str(), QUrl());
    QQuickItem* imageItem = qobject_cast<QQuickItem*>(component.create());

    if (!imageItem) {
        qDebug() << component.errorString();
        return nullptr;
    }

    imageItem->setWidth(img.item.width);
    imageItem->setHeight(img.item.height);
    imageItem->setX(img.item.x);
    imageItem->setY(img.item.y);
    imageItem->setZ(img.item.z);
    imageItem->setOpacity(img.item.opacity);
    imageItem->setScale(img.item.scale);
    imageItem->setRotation(img.item.rotation);
    imageItem->setVisible(img.item.visible);
    imageItem->setAntialiasing(img.item.antialiasing);

    imageItem->setProperty("source", img.source);
    imageItem->setProperty("sourceSize", QSize(img.sourceSizeWidth, img.sourceSizeHeight));
    imageItem->setProperty("asynchronous", img.asynchronous);
    imageItem->setProperty("autoTransform", img.autoTransform);
    imageItem->setProperty("cache", img.cache);
    imageItem->setProperty("mipmap", img.mipmap);
    imageItem->setProperty("mirror", img.mirror);

    return imageItem;
}

void QmlParser::printMetaData()
{
    // For debugging purposes only
    printTraversal(m_qmlRootItem);
}

void QmlParser::printTraversal(QObject* rootObject)
{
    // For debugging purposes only
    if(!rootObject)
        return;

    QString className = rootObject->metaObject()->className();
    qDebug() << "  className: " << className << " : " << rootObject;
    qDebug() << "  propertyList: ";

    for(int i=0; i<rootObject->metaObject()->propertyCount(); i++) {
         QMetaProperty prop = rootObject->metaObject()->property(i);
         QVariant propertyValue = prop.read(rootObject);
         qDebug() << "     " << i << "propertyName: " << prop.name() << " ; propertyValue: " << propertyValue << " ("<< propertyValue.isValid() <<")";
    }
    foreach(QObject* child, rootObject->children())
        printTraversal(child);
}

void QmlParser::addItem(QQuickItem* item)
{
//     For testing purposes only
    TextMetadata text;
    text.item.width = 200;
    text.item.height = 200;
    text.item.x = 50;
    text.item.y = 60;
    text.fontPointSize = 100;
    text.fontWeight = "Font.Thin";
    text.text = "THIS MA TEXT NIGGA\n";
    text.fontPointSize = 50.0;
    text.item.visible = true;
    text.item.antialiasing = false;
    text.item.scale = 1;
    text.item.opacity = 1;
    text.wrapMode = "Text.Wrap";
    text.horizontalAlignment = "Text.AlignRight";
    text.verticalAlignment = "Text.AlignBottom";
    text.fontUnderline = true;
    text.fontStrikeout = true;
    text.color = "pink";

    QQuickItem *textItem = createTextItem(text);
    textItem->setParentItem(item);
    textItem->setParent(item);


//    //   Add a dummy pink rectangle
//    struct RectangleMetadata rectItem;
//    rectItem.item.x = 25;
//    rectItem.item.y = 25;
//    rectItem.item.z = 0;
//    rectItem.item.width = 50;
//    rectItem.item.height = 50;
//    rectItem.color = "pink";
//    rectItem.item.opacity = 1;
//    rectItem.item.scale = 1;
//    rectItem.borderWidth = 2;
//    rectItem.borderColor = "red";
//    rectItem.gradientData="1,0.0,pink,1.0,purple";
//    rectItem.item.visible = true;
//    rectItem.item.antialiasing = false;
//    rectItem.radius = 0;
//    QQuickItem *rectangleItem = createRectangle(rectItem);
//    rectangleItem->setParentItem(item);
//    rectangleItem->setParent(item);

//    struct ImageMetadata img;
//    img.item.width = 300;
//    img.item.height = 300;
//    img.source = "/home/akhilkg/mee.jpg";
//    img.fillMode = "Image.Tile";
//    img.mirror = true;

}
