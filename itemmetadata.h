#ifndef ITEMMETADATA_H
#define ITEMMETADATA_H

#include <QObject>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QDebug>
#include <QQuickItem>
#include <QAnimationGroup>
#include <QQuickView>
#include <QQmlContext>
#include <QJSValue>
#include <QQmlListProperty>
#include <QQuickTransform>
#include <QFontInfo>

QVariant getValue(QObject* rootObject, const QString &propertyName, int propertyIndex);

struct TransformMetadata
{
    struct Rotation
    {
        // we use float and not qreal because metaObject() returns a QVector3D for "transform" which operates in float
        float axisX = 0.0;
        float axisY = 0.0;
        float axisZ = 0.0;
        float originX = 0.0;
        float originY = 0.0;
        float angle = 0.0;
    } rotation;

    struct Scale
    {
        float originX = 0.0;
        float originY = 0.0;
        float xScale = 1.0;
        float yScale = 1.0;
    } scale;

    struct Translate
    {
        float x = 0.0;
        float y = 0.0;
    } translate;

    //TODO: add Matrix4x4 ?

    void init(QObject* transform);

    // Current implementation only considers one instance of Rotation, Scale and Translate each in a transform group.
    void transformTraversal(QObject* transform);
    /* Format:
     * Rotation.axisX,Rotation.axisY,Rotation.axisZ,Rotation.originX,Rotation.originY,Rotation.angle
     * Scale.originX,Scale.originY,Scale.xScale,Scale.yScale
     * Translate.x,Translate.y
     * */
    QString prepareFormat();
};

// TODO: Add anchors to QmlParser::create*Item() methods
// Note that it seems that anchors cannot be accessed through QMetaObject because QQuickAnchorLine isn't a QObject-inherited class
struct AnchorsGroup {
    // This order is preserved while writing/reading metadata
    qreal margins = 0.0;
    qreal topMargin = 0.0;
    qreal bottomMargin = 0.0;
    qreal leftMargin = 0.0;
    qreal rightMargin = 0.0;
    qreal baselineOffset = 0.0;
    qreal horizontalCenterOffset = 0.0;
    qreal verticalCenterOffset = 0.0;
    QString fill = "";
    QString centerIn = "";
    bool alignWhenCentered = true;

    void init(QObject *rootObject);
    QString prepareFormat();
};

/* We write the metadata for QQuickItem in this format:
 *
 * className,QQuickItem
 * childrenCount,childCount
 * anchorsGroup,
 * transform,
 * values_int,
 * values_real,x,y,z,width,height,opacity,scale,rotation,
 * values_string,transformOrigin,
 * values_bool,visible,antialiasing,smooth,
 *
 * */
struct ItemMetadata
{
    QString className = "-1";
    // Note: "id" is actually "objectName" since it is not possible to query the id of a QQuickItem from QML.
    QString id = "-1";
    QString parent = "-1";
    QString transformOrigin = "Item.Center";
    int childCount = 0;
    qreal x = 0.0;
    qreal y = 0.0;
    qreal z = 0.0;
    qreal width = 0.0;
    qreal height = 0.0;
    qreal opacity = 1.0;
    qreal scale = 1.0;
    qreal rotation = 0.0;

    bool visible = true;
    bool antialiasing = false;
    bool smooth = true;
    // TODO: add transform

    struct TransformMetadata transform;
    struct AnchorsGroup anchors;

    void init(QObject* rootObject);
    QString prepareFormat();
};


/* We write the metadata for QQuickRectangle in this format:
 *
 * className,QQuickRectangle
 * childrenCount,childCount
 * anchorsGroup,
 * transform,
 * values_int,
 * values_real,x,y,z,width,height,opacity,scale,rotation,
 * values_string,
 * values_bool,visible,antialiasing,
 * gradient
 * valus_int,border.width,
 * values_real,radius,
 * values_string,color,border.color
 * vales_bool,
 * */
struct RectangleMetadata
{
    ItemMetadata item;
    int borderWidth;
    qreal radius;
    QString color;
    QString borderColor;
    QString gradientData;

    void init(QObject* rootObject);

    /*
     * \qml
     *   gradient: Gradient {
     *      GradientStop { position: 0.0; color: "pink" }
     *      GradientStop { position: 1.0; color: "purple" }
     *   }
     *  \endqml
     *
     *  The QML Tree for the above qml looks like this:
     *     QQuickGradient
     *          |
     *          |
     *          |
     *     -----------------------
     *     |                     |
     * QQuickGradientStop   QQuickGradientStop
     * (0.0, "pink")          (1.0, "purple")
     *
     * parseGradient(QObject*) calls gradientTraversal(QObject*)
     * which iterates recursively to
     * obtain this value pair and returns a QString containing the concatenated
     * value pair. The orientation is prefixed to this string.
     * For the above example, parseGradientObject(QObject*) returns:
     * "Gradient.Vertical,0.0,pink,1.0,purple,"
     * */
    QString parseGradient(QObject* rootObject);
    QString gradientTraversal(QObject* gradient);
    QString prepareFormat();
};

/* We write the metadata for QQuickText in this format:
 *
 * className,QQuickText
 * childrenCount,childCount
 * anchorsGroup,
 * transform,
 * values_int,
 * values_real,x,y,z,width,height,opacity,scale,rotation,
 * values_string,
 * values_bool,visible,antialiasing,
 * text
 * values_int,fontPixelSize,
 * values_real,fontPointSize,lineHeight,fontLetterSpacing,fontWordSpacing,padding,
 *              bottomPadding,topPadding,leftPadding,rightPadding,
 * values_string,color,elide,fontFamily,fontSizeMode,fontCapitalization,style,styleColor,
 *              text,textFormat,wrapMode,horizontalAlignment,verticalAlignment,renderType,fontWeight
 *              fontLoaderName,fontLoaderSource,
 * values_bool,fontBold,fontItalic,fontStrikeout,fontUnderline,fontKerning,fontPreferShaping,ifFontLoader,
 * */
struct TextMetadata
{
    ItemMetadata item;
    int fontPixelSize = -1;
    qreal fontPointSize = 10.0;
    qreal lineHeight = 1.0;
    qreal fontLetterSpacing = 0.0;
    qreal fontWordSpacing = 0.0;
    qreal padding = 0.0;
    qreal bottomPadding = 0.0;
    qreal topPadding = 0.0;
    qreal leftPadding = 0.0;
    qreal rightPadding = 0.0;

    QString color = "#000000";
    QString elide = "Text.ElideNone";
    QString fontFamily = "Noto Sans";
    QString fontSizeMode = "Text.FixedSize";
    QString fontCapitalization = "Font.MixedCase";
    QString style = "Text.Normal";
    QString textFormat = "Text.AutoText";
    QString wrapMode = "Text.NoWrap";
    QString horizontalAlignment = "Text.AlignLeft";
    QString verticalAlignment  = "Text.AlignTop";
    QString renderType = "Text.QtRendering";
    QString fontWeight = "Font.Normal";
    QString styleColor;
    QString text;
    QString fontLoaderName;
    QString fontLoaderSource;

    bool fontBold = false;
    bool fontItalic = false;
    bool fontStrikeout = false;
    bool fontUnderline = false;
    bool fontKerning = false;
    bool fontPreferShaping = true;
    bool ifFontLoader = false;

    // not supported/disregarded enums
    /*
     * QString lineHeightMode;
     * QString hintingPreference;
     * QString fontStyleName  = "Font.Normal"; // forsaken; use italics, bold and underline instead :)
    */

    void init(QObject* rootObject);
    QString prepareFormat();
};

/* We write the metadata for QQuickImage in this format:
 * className,QQuickText
 * childrenCount,childCount
 * anchorsGroup,
 * transform,
 * values_int,
 * values_real,x,y,z,width,height,opacity,scale,rotation,
 * values_string,
 * values_bool,visible,antialiasing,
 * values_int,sourceSizeWidth,sourceSizeHeight
 * values_real,
 * values_string,source,horizontalAlignment,verticalAlignment,fillMode,
 * values_bool,asynchronous,autoTransform,cache,
 *              mipmap,mirror,
 *
 * */
struct ImageMetadata
{
    ItemMetadata item;
    int sourceSizeWidth = -1;
    int sourceSizeHeight = -1;
    QString source;
    QString horizontalAlignment = "Image.AlignLeft";
    QString verticalAlignment  = "Image.AlignTop";
    QString fillMode = "Image.Stretch";
    bool asynchronous = false;
    bool autoTransform = false;
    bool cache = true;
    bool mipmap = false;
    bool mirror = false;

    void init(QObject* rootObject);
    QString prepareFormat();
};

#endif // ITEMMETADATA_H
